import { createRouter, createWebHistory } from 'vue-router'
import HomeView from '../views/HomeView.vue'
import RandomTww from '../views/RandomTww.vue';
import Random from '../views/Random.vue';

const router = createRouter({
    history: createWebHistory(import.meta.env.BASE_URL),
    routes: [
        {
            path: '/',
            name: 'home',
            component: HomeView
        },
        {
            path: '/random_tww',
            name: 'random_tww',
            component: () => RandomTww
        },
        {
            path: '/random',
            name: 'random',
            component: () => Random
        }
    ]
})

export default router
