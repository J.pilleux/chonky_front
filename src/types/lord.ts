export default interface Lord {
    name: String,
    faction: String,
    game: Number,
}
